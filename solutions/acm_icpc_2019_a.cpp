#include <iostream>
#include <vector>
#include <cassert>
#include <map>

using namespace std;

// Давайте назовём типы данных, чтобы было проще
// Нам нужна структура, в которой будут лежать отсортированные по цене блоки,
// в каждом блоке - отсортированные по высоте плитки (формально, их номера)

// Все плитки из одного ряда с одинаковой ценой, отсортированные по высоте.
// Ключ - высота плитки, значение - порядковый номер
// Используем multimap т.к. может быть несколько плиток одинаковой цены и высоты
typedef multimap<int, size_t> Block;
// Все блоки из одного ряда, отсортированные по цене.
// Ключ - цена, значение - блок.
typedef map<int, Block> Row; 

// Функция, которая считывает массив и возвращает его
// Мы могли бы сделать шаблонный метод, который умеет читать не только числа,
// но и любые другие типы данных, но в этой задаче нам это не нужно
vector<int> read_vector(size_t n_elements) {
	vector<int> result(n_elements); // Создаём массив длины n_elements 
	for (size_t i = 0; i < n_elements; ++i) {
		cin >> result[i];
	}
	return result;
}

// Функция, которая печатает массив. Опять-таки не будем делать её слишком универсальной
void print_vector(const vector<size_t>& v) {
	for (size_t i = 0; i < v.size(); ++i) {
		cout << v[i] << " ";
	}
	cout << endl;
}

// Функция, которая превращает массивы цен и высот плиток в упорядоченную по
// цене и высоте структуру
void fill_row(Row& row, const vector<int>& prices, const vector<int>& heights) {
	assert(prices.size() == heights.size()); // На всякий случай
	size_t n_tiles = prices.size();
	for (size_t i = 0; i < n_tiles; ++i) {
		// Если у нас ещё нет блока для соответствующей цены, то добавим его
		if (row.find(prices[i]) == row.end()) {
			row[prices[i]] = Block();
		}

		// Добавим плитку в блок (делаем + 1 т.к. нам в ответе нужна нумерация с единицы)
		row[prices[i]].insert(make_pair(heights[i], i + 1));
	}
}

// Функция, которая выбирает самую маленькую подходящую плитку из заднего блока,
// при фиксированной передней плитке
Block::iterator best_back_tile(Block::iterator front_tile, Block& back_block) {
	int front_tile_height = front_tile->first;
	return back_block.upper_bound(front_tile_height);
}

// Функция, которая выбирает самую большую подходящую плитку из переднего блока,
// при фиксированной задней плитке
Block::iterator best_front_tile(Block::iterator back_tile, Block& front_block) {
	int back_tile_height = back_tile->first;
	// Т.к. в С++ нет нужной функции, то тут будет чуть-чуть сложнее:
	// Найдём первую плитку в переднем ряду, которая не ниже задней
	Block::iterator front_tile = front_block.lower_bound(back_tile_height);
	// И теперь возьмём предыдущую, если она есть, иначе вернём пустой итератор:
	if (front_tile != front_block.begin()) {
		--front_tile;
	} else {
		front_tile = front_block.end();
	}
	return front_tile;
}

// Функция проверяет, не пустой ли первый блок, и если да, то удаляет его
void remove_empty_first_block(Row& row) {
	if (row.empty()) { // Выходим, если не ни одного блока
		return;
	}

	// Удаляем первый блок, если он пустой
	Row::iterator block = row.begin();
	if (block->second.empty()) {
		row.erase(block);
	}
} 

int main() {
	size_t n_tiles;
	cin >> n_tiles;

	// Чтение массива вынесли в отдельную функцию, чтобы не дублировать код
	// Дублирование кода - зло. Всегда.
	vector<int> back_prices = read_vector(n_tiles);
	vector<int> back_heights = read_vector(n_tiles);
	vector<int> front_prices = read_vector(n_tiles);
	vector<int> front_heights = read_vector(n_tiles);

	Row back_row, front_row; // Два ряда плиток
	fill_row(back_row, back_prices, back_heights);
	fill_row(front_row, front_prices, front_heights);

	vector<size_t> back_answer, front_answer; // Массивы, в которых будем хранить ответ.
	
	// А теперь, собственно, решение
	for(size_t i = 0; i < n_tiles; ++i) {
		Block& back_block = back_row.begin()->second; // Самый дешёвый блок первого ряда
		Block& front_block = front_row.begin()->second; // Самый дешёвый блок второго ряда
		Block::iterator front_tile, back_tile; // Итераторы, которые будут указывать на плитку в
			// переднем и плитку в заднем ряду, которую мы хотим поставить.
		
		if (front_block.size() <= back_block.size()) { // Если блок в ближнем ряду короче или такой же, то...
			front_tile = front_block.begin(); // ... мы берём любую плитку из ближнего блока (например первую), ...
			back_tile = best_back_tile(front_tile, back_block); // ... и находим самую подходящую из дальнего		
		} else { // Если же блок в дальнем ряду короче (соедржит меньше плиток), то наоборот:
			back_tile = back_block.begin(); // берём любую плитку из заднего блока ...
			front_tile = best_front_tile(back_tile, front_block); // ... и находим самую подходящую из переднего
		}

		if (back_tile == back_block.end() || front_tile == front_block.end()) {
			// Если вместо одной из плиток пустой итератор, печатаем отрицательный ответ и выходим.
			cout << "impossible" << endl;
			return 0;
		}

		// Итак, мы нашли плитки, которые поставим на полку. Добавим их номера к ответу:
		front_answer.push_back(front_tile->second);
		back_answer.push_back(back_tile->second);

		// И удалим из их блоков
		front_block.erase(front_tile);
		back_block.erase(back_tile);

		// Если какой-либо из блоков оказался пустой, удалим его из ряда:
		remove_empty_first_block(front_row);
		remove_empty_first_block(back_row);
	}

	// Ну и напечатаем ответ:
	print_vector(back_answer);
	print_vector(front_answer);

	return 0;
}
